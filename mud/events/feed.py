# -*- coding: utf-8 -*-
# Copyright (C) 2018 Etienne Fouquet
#==============================================================================

from .event import Event2, Event3

class FeedEvent(Event2):
    NAME = "feed"

    def perform(self):
        self.inform("feed")


class FeedWithEvent(Event3):
    NAME = "feed-with"

    def perform(self):
        self.inform("feed-with")
