from .action import Action3
from mud.events import CookWithEvent

class CookWithAction(Action3):
    EVENT = CookWithEvent
    ACTION = "cook"
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_operate"
